from datetime import datetime
from pathlib import Path
from typing import Dict
import logging

from pony.orm import *

from exifdb.Config import Config

db = Database('sqlite', str(Config().project_root / 'data' / 'exif.db'), create_db=True)


class Pic(db.Entity):
    id = PrimaryKey(int, auto=True)
    path = Required(str, unique=True)
    inserted_at = Required(datetime, sql_default='CURRENT_TIMESTAMP')
    updated_at = Optional(datetime)
    pic_exifs = Set('PicExif')


class PicExif(db.Entity):
    pic = Required(Pic)
    exif_key = Required(str)
    exif_value = Optional(str)
    inserted_at = Required(datetime, sql_default='CURRENT_TIMESTAMP')
    updated_at = Optional(datetime)
    PrimaryKey(pic, exif_key)


# sql_debug(True)
db.generate_mapping(create_tables=True)


class DbWriter:
    db_location: Path

    def __init__(self):
        pass

    @db_session
    def add_picture(self, path: str):
        logging.info(f'adding {path}')
        Pic(path=path)

    @db_session
    def pic_exists(self, path: str):
        return Pic.exists(path=path)

    @db_session
    def add_picture_exif(self, path: str, exif: Dict):
        pic = Pic.get(path=path)
        for item in exif.items():
            key = item[0]
            val = item[1]
            print(key, val)
            PicExif(pic=pic, exif_key=key, exif_value=str(val))
