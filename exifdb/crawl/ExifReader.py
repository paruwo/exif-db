import glob
import logging
from logging import Logger
from pathlib import Path
from typing import Iterable, Dict, Any

from exif import Image

from exifdb.Config import Config
from exifdb.crawl.DbWriter import DbWriter


class ExifReader:
    def __init__(self):
        self.suffixes = Config().exif_suffixes
        self.log = Logger(name='ExifReader')

    def scan_folder(self, folder: Path) -> Iterable[Path]:
        for _image in glob.glob(str(folder / '*.*')):
            img = Path(_image)
            if Path(img).suffix[1:].upper() not in self.suffixes:
                self.log.info('filtering out ' + str(img))
                continue

            self.log.info(f'found {img}')
            yield img

    @staticmethod
    def read_exif(file: Path) -> Dict[str, Any]:
        with file.open('rb') as image_file:
            return Image(image_file).get_all()


# just for testing anything
if __name__ == '__main__':
    logging.basicConfig()  # encoding='utf-8', level=logging.DEBUG
    r = ExifReader()
    folder = Path('<path>')

    w = DbWriter()

    for i in r.scan_folder(folder=folder):
        if not w.pic_exists(path=str(i)):
            w.add_picture(path=str(i))
            w.add_picture_exif(path=str(i), exif=ExifReader.read_exif(i))
        else:
            logging.info(f'{i} already exists')

