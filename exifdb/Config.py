from pathlib import Path

import toml
from py_singleton import singleton


@singleton
class Config:
    project_root: Path

    def __init__(self):
        self.project_root = Path(__file__).parents[1]
        self._parse_config_file()

    def _parse_config_file(self):
        config_file = Path(self.project_root, 'config.toml')
        config_parser = toml.load(f=config_file)

        # general
        self.log_level = config_parser['general']['loglevel']

        # exif
        self.exif_suffixes = config_parser['exif']['suffixes']
