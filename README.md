# My EXIF database

### What?

A small tool for you to track and analyze your photo's EXIF data.
All your data belongs to you, so you can configure it.

### Why?

I am a convinced [CaptureOne](https://www.captureone.com) user
but prefer using it on my Workstation only. The reason is that
its catalog/index is awesome but should be residing on a local
disk only and not on a shared NAS drive - for strong performance
reasons - and that's ok.

Additionally, to learn about own preference or favourite gear
over time is not a core functionality of photo editing software.

And, another micro project to practise more Python.


# Developer Notes

Related music is from [Oranssi Pazuzu](https://www.youtube.com/watch?v=PfANRKPnC8M).

For now the backend is Pony ORM on sqlite.

Setup for development:

    pip install .[test,quality]

QA check:

    # code checks
    flake8 exifdb
    
    # code coverage
    coverage run -m pytest
    coverage html


# Implementation Ideas

 * Usage
    * Make is lightweight and enjoyable
    * Easy to install, configure and use (clone + pip install .)
    * Web-based GUI possible (see pgadmin4)
 * Data
    * No cloud, local only
    * Crawl and create catalog
      * Non-blocking and parallel read, single local writer process
      * Keep: absolute path, all exif tags & values, executed analyses
    * Incremental update
      * check for existence in local catalog
      * check last creation/modification timestamp
    * Compress and encrypt all data
    * No native SMB needed for now but just access mounts (no password management)
 * Few first ideas on questions to answer
    * Favourite lenses / focal length / ...
    * Usage of X over time / season
